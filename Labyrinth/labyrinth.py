import sys

debug = False


class Maze(object):
    class Directions(dict):
        def __init__(self):
            self["LEFT"] = lambda x: (x[0], x[1] - 1)
            self["DOWN"] = lambda x: (x[0] + 1, x[1])
            self["RIGHT"] = lambda x: (x[0], x[1] + 1)
            self["UP"] = lambda x: (x[0] - 1, x[1])

        def next_direction(self, current_direction):
            return self.keys()[(self.keys().index(current_direction) + 1) % 4]

        def previous_direction(self, current_direction):
            return self.keys()[(self.keys().index(current_direction) - 1) % 4]

        def opposite(self, current_direction):
            return self.keys()[(self.keys().index(current_direction) + 2) % 4]

    class Cell(object):
        def __init__(self, _type, _coordinates):
            self.type = _type
            self.visited = False
            self.coordinates = _coordinates

            self.parent_direction = None
            self.g_cost = None
            self.h_heuristic = None

        def __str__(self):
            return self.type

        def update(self, _type):
            self.type = _type

        def visit(self):
            self.visited = True

        def f_expected_cost(self):
            return self.g_cost + self.h_heuristic

        def manhattan_distance(self, destination):
            return abs(self.coordinates[0] - destination.coordinates[0]) + abs(
                self.coordinates[1] - destination.coordinates[1])

        def neighbour(self, direction):
            return maze.get_cell(Maze.directions[direction](self.coordinates))

        def safe_moves(self, safe_cell_types):
            return [direction for direction in Maze.directions.keys()
                    if self.neighbour(direction).type in safe_cell_types]

        def unvisited_neighbours(self, safe_cell_types):
            return [direction for direction in Maze.directions.keys()
                    if direction in self.safe_moves(safe_cell_types) and
                    self.neighbour(direction).visited is False]

    directions = Directions()

    def __init__(self, _rows, _columns, _countdown, _initial_x, _initial_y):
        print >> sys.stderr, _rows, _columns, _countdown, '\n', _initial_x, _initial_y
        self.initial_coordinates = (_initial_x, _initial_y)
        self.rows = _rows
        self.columns = _columns
        self.countdown = _countdown
        self.visited_maze = [[Maze.Cell('?', (x, y)) for y in range(self.columns)] for x in range(self.rows)]
        self.chamber_coordinates = None
        self.alarm_triggered = False
        self.best_known_escape_path_length = float("inf")
        self.safe_cell_types = [".", "T"]

    def get_cell(self, coordinates):
        return self.visited_maze[coordinates[0]][coordinates[1]]

    def update(self, kirk_x, kirk_y):
        if debug:
            if self.visited_maze[kirk_x][kirk_y].type == '#':
                raise Exception("Failure: Kirk hit a trap in the wall and died.")
            if self.countdown == 0:
                raise Exception(
                    "Failure: Kirk triggered the alarm and did not reach the exit on time. He was killed by a machine")
            if self.alarm_triggered:
                self.countdown -= 1
            for x in range(kirk_x - 2, kirk_x + 3):
                for y in range(kirk_y - 2, kirk_y + 3):
                    if 0 <= x < self.rows and 0 <= y < self.columns:
                        self.visited_maze[x][y].update(Maze.full_maze[x][y])
                        if Maze.full_maze[x][y] == "C":
                            self.chamber_coordinates = (x, y)
        else:
            for i in xrange(self.rows):
                row = raw_input()
                print >> sys.stderr, row
                if self.chamber_coordinates is None and "C" in row:
                    j = row.index("C")
                    self.chamber_coordinates = (i, j)
                for j, cell_type in enumerate(row):
                    self.visited_maze[i][j].update(cell_type)
                    # C of the characters in '#.TC?' (i.e. one line of the ASCII maze).

    def update_best_path(self):
        if self.chamber_coordinates is not None and self.best_known_escape_path_length > self.countdown:
            chamber_cell = self.get_cell(self.chamber_coordinates)
            initial_cell = self.get_cell(self.initial_coordinates)
            self.best_known_escape_path_length = len(
                self.a_star_pathfinding(chamber_cell, initial_cell, safe_cell_types=[".", "T", "C"]))
            if self.best_known_escape_path_length <= self.countdown:
                self.safe_cell_types = [".", "T", "C"]

    def a_star_pathfinding(self, source, destination=None, safe_cell_types=[".", "T"]):
        source.g_cost = 0
        source.h_heuristic = 0
        open_set = {source}
        closed_set = set([])

        while len(open_set) > 0:
            q = min(open_set, key=lambda cell: cell.f_expected_cost())
            open_set.remove(q)
            successors = [(self.get_cell(Maze.directions[direction](q.coordinates)), direction) for
                          direction in q.safe_moves(safe_cell_types)]
            for successor in successors:
                if successor[0] not in closed_set:
                    total_cost = q.g_cost + 1
                    if destination is not None:
                        total_cost += successor[0].manhattan_distance(destination)
                    if successor[0] not in open_set.union(closed_set) or \
                            (successor[0] in open_set.union(closed_set) and successor[0].f_expected_cost() > total_cost):
                        successor[0].parent_direction = Maze.directions.opposite(successor[1])
                    if destination is None:
                        if successor[0].visited is False:
                            return successor[0]
                    elif successor[0] == destination:
                        return self.recreate_path(source, destination)
                    successor[0].g_cost = q.g_cost + 1
                    if destination is not None:
                        successor[0].h_heuristic = successor[0].manhattan_distance(destination)
                    else:
                        successor[0].h_heuristic = 0
                    open_set.add(successor[0])
            closed_set.add(q)
        return []

    def recreate_path(self, source, destination):
        path = []
        while destination != source:
            path.append(Maze.directions.opposite(destination.parent_direction))
            destination = self.get_cell(Maze.directions[destination.parent_direction](destination.coordinates))
        return path

    full_maze = None
    if debug:
        full_maze = """??????????????????????????????
??????????????????????????????
??????????????????????????????
??????????????????????????????
???############???????????????
???############???????????????
???##T......C##???????????????
???############???????????????
???############???????????????
??????????????????????????????
??????????????????????????????
??????????????????????????????
??????????????????????????????
??????????????????????????????
??????????????????????????????""".split('\n')
        full_maze = [list(row) for row in full_maze]


class Kirk(object):
    def __init__(self, _maze):
        self.maze = _maze
        self.current_cell = self.maze.get_cell(_maze.initial_coordinates)
        self.moves_count = 0
        self.last_direction = None
        self.destination = None
        self.path = []
        self.maze.get_cell(self.maze.initial_coordinates).visit()

    # Kirk's next move (UP DOWN LEFT or RIGHT).
    def move(self):
        self.moves_count += 1
        if debug and self.moves_count == 1200:
            raise Exception("Failure: Kirk's jetpack is out of fuel. He hit the ground and was killed by a machine")

        if self.current_cell.type == "C":
            self.trigger_alarm()

        if self.current_cell.unvisited_neighbours(safe_cell_types=[".", "T"]) == []:
            self.destination = self.find_destination_coordinates()

        if self.destination is None:
            self.boldly_go_where_no_man_has_gone_before()
        else:
            self.head_for_destination_coordinates()
        if self.current_cell.neighbour(self.last_direction).visited is False:
            self.maze.update_best_path()
            self.current_cell.neighbour(self.last_direction).visit()

    def find_destination_coordinates(self):
        initial_cell = self.maze.get_cell(self.maze.initial_coordinates)
        chamber_cell = None
        if self.maze.chamber_coordinates is not None:
            chamber_cell = self.maze.get_cell(self.maze.chamber_coordinates)

        if self.maze.alarm_triggered:
            return initial_cell
        elif self.maze.chamber_coordinates is not None and \
                self.maze.best_known_escape_path_length <= self.maze.countdown:
            return chamber_cell
        else:
            return self.maze.a_star_pathfinding(self.current_cell)

    def boldly_go_where_no_man_has_gone_before(self):
        proposed_direction = self.last_direction or "RIGHT"
        while proposed_direction not in self.current_cell.unvisited_neighbours(safe_cell_types=[".", "T"]):
            proposed_direction = self.maze.directions.next_direction(proposed_direction)

        self.last_direction = proposed_direction
        print proposed_direction
        print >> sys.stderr, proposed_direction, "Boldly\n"

    def head_for_destination_coordinates(self):
        if self.path == []:
            self.path = self.find_path(self.maze.safe_cell_types)
            self.path = list(reversed(self.path))
        direction = self.path.pop(0)

        self.last_direction = direction
        print direction
        print >> sys.stderr, direction, "Destination\n"

        if self.path == []:
            self.destination = None

    def find_path(self, safe_cell_types):
        return self.maze.a_star_pathfinding(self.current_cell, self.destination, safe_cell_types=safe_cell_types)

    def trigger_alarm(self):
        self.maze.alarm_triggered = True
        self.destination = self.maze.get_cell(self.maze.initial_coordinates)
        self.path = []


def game_loop(player, maze):
    while not (maze.alarm_triggered and player.current_cell.coordinates == maze.initial_coordinates):
        if player.moves_count > 0:
            if debug:
                player.current_cell = player.current_cell.neighbour(player.last_direction)
            else:
                player.current_cell = maze.get_cell(tuple([int(i) for i in raw_input().split()]))
            print >> sys.stderr, player.current_cell.coordinates

        maze.update(*player.current_cell.coordinates)
        if maze.alarm_triggered and player.current_cell.coordinates == maze.initial_coordinates:
            return
        player.move()


if debug:
    # reads data from Maze.full_maze
    maze = Maze(len(Maze.full_maze), len(Maze.full_maze[0]), int(raw_input()),
                *[(ix, iy) for ix, x in enumerate(Maze.full_maze) for iy, y in enumerate(x) if y == "T"][0])
else:
    # reads first two lines, passes them to __init__
    maze = Maze(*([int(i) for i in raw_input().split()] + [int(i) for i in raw_input().split()]))

kirk = Kirk(maze)

game_loop(kirk, maze)
